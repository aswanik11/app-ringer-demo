<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BooksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::post('/user-login', [AuthController::class, "userLogin"]);
Route::post('/user-register', [AuthController::class, "userRegister"]);

Route::get('/dashboard', function () {
    return view("dashboard");
});
Route::get('/logout', [AuthController::class, "logout"]);

//Books Route
Route::get('/book-list', [BooksController::class, "bookList"]);
Route::get('/save-book-info', [BooksController::class, "saveBookInfo"]);
