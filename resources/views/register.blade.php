<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>User Registration </title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  
</head>
<body>
  <div class="container">
    <!-- main app container -->
      <div class="container">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <h3 class="my-5">Register</h3>

            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                <i class="fa fa-check"></i> {{ $error }}
                @endforeach
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
              <<symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
              </symbol>
            </svg>
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger d-flex align-items-center" role="alert">
              <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
              <div>
                {{ $error }}
              </div>
            </div>
            @endforeach
            
            @endif
            <form method="post" id="handleAjax" action="{{url('do-register')}}" name="postform">
              <div class="mb-3">
                <label>Name</label>
                <input type="text" name="name" value="{{old('name')}}" class="form-control" required />
              </div>
              <div class="mb-3">
                <label>Email</label>
                <input type="email" name="email" value="{{old('email')}}" class="form-control" required />
                @csrf
              </div>
              <div class="mb-3">
                <label>Password</label>
                <input type="password" name="password" class="form-control" required />
              </div>
              <div class="mb-3">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" required />
              </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">REGISTER</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <div class="text-center">
      <p>
        <a href="{{ url('login') }}" target="_top">Already registered? Login Here</a>
      </p>
      <p>
        <a href="{{ url('book-list') }}" target="_top">Book List</a>
      </p>
    </div>
  </div>
</body>

</html>