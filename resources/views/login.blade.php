<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>User Login </title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>
<body>
  <div class="container">
    <!-- main app container -->
      <div class="container">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <h3 class="my-5">Login</h3>
            @if (\Session::has('success'))
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
              <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
              </symbol>
            </svg>
            <div class="alert alert-success d-flex align-items-center" role="alert">
              <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
              <div>
                {!! \Session::get('success') !!}
              </div>
            </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" id="handleAjax" action="{{url('user-login')}}" name="postform">
              <div class="mb-3">
                <label>Email</label>
                <input type="email" name="email" value="{{old('email')}}"  class="form-control" />
                @csrf
              </div>
              <div class="mb-3">
                <label>Password</label>
                <input type="password" name="password"   class="form-control" />
              </div>
              <div class="mb-3"></div>
                <button type="submit" class="btn btn-primary">LOGIN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
   
    <div class="text-center">
      <p>
        <a href="{{ url('register') }}" target="_top">Don't have an account? Register Here</a>
      </p>
      <p>
        <a href="{{ url('book-list') }}" target="_top">Book List</a>
      </p>
    </div>
  </div>
    
</body>

</html>