<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Books List </title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
  {{-- <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" /> --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/r-2.3.0/datatables.min.css"/>

</head>
<body>
  <div class="container">
    <!-- main app container -->
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="my-5">Books List using DataTable</h3>
            <a href="/save-book-info" class="btn btn-primary my-4">Save Books</a>
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive mb-5">
                <table class="table table-hover m-0 table-centered dt-responsive nowrap w-100" id="bookTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Subtitle</th>
                        <th scope="col">Authors</th>
                        <th scope="col">SmallThumbnail</th>
                        <th scope="col">Thumbnail</th>

                    </tr>
                    </thead>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>
    
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/r-2.3.0/datatables.min.js"></script>

  <script>
   $(document).ready(function () {
    $.ajax({
      url: "https://run.mocky.io/v3/821d47eb-b962-4a30-9231-e54479f1fbdf",
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "AppRinger");
      },
      success: function (response) {
        // let response = JSON.parse(res);
        $("#bookTable").DataTable({
        processing: true,
        data: response.items,
        columns: [
            { data: "null", defaultContent: `` },
            { data: "id" },
            { data: "volumeInfo.title" },
            { data: "null", defaultContent: `` },
            { data: "volumeInfo.authors[0]" },
            { data: "volumeInfo.imageLinks.smallThumbnail" },
            { data: "volumeInfo.imageLinks.thumbnail" },
        ],
        columnDefs: [
          {
            targets: 3,
            render: function (data, type, row) {
                if(row.volumeInfo.subtitle != ""){
                    return row.volumeInfo.subtitle;
                }
              
            },
          },
        ],
          rowCallback: function (row, data, index, DisplayIndexFull) {
            var index = DisplayIndexFull + 1;
            $("td:first", row).html(index);
            return row;
          },
        });
      },
    });
});

  </script>
</body>

</html>