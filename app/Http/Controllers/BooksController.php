<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Books;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class BooksController extends Controller
{
    protected $request;
    protected $data;

    public function __construct(Request $request)
    {

        $this->request = $request;
        $this->data = [];
    }
    function bookList(Request $request)
    {
        return view("books");
    }

    public function saveBookInfo(Request $request)
    {
        // fetch the book info from api
        $client = new Client();
        $url = "https://run.mocky.io/v3/821d47eb-b962-4a30-9231-e54479f1fbdf";

        $response = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => "AppRinger"
            ]
        ]);
        $result = json_decode($response->getBody(), true);
        foreach ($result['items'] as $val) {
            $bookInfo['id'] = $val['id'];
            $bookInfo['title'] = $val['volumeInfo']['title'];
            if (isset($val['volumeInfo']['subtitle']) != "") {
                $bookInfo['subtitle'] = $val['volumeInfo']['subtitle'];
            } else {
                $bookInfo['subtitle'] = "";
            }
            $bookInfo['authors'] = $val['volumeInfo']['authors']['0'];
            $bookInfo['smallThumbnail'] = $val['volumeInfo']['imageLinks']['smallThumbnail'];
            $bookInfo['thumbnail'] = $val['volumeInfo']['imageLinks']['thumbnail'];
            //insert data
            $books = \App\Models\Books::create($bookInfo);
        }
        $arrResponse['http_status'] = 200;
        $arrResponse['message'] = "Book info saved into database successfully!";
        return response()->json($arrResponse, 200);
    }
}
